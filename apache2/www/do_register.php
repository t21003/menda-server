<?php
//ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

$mysqli = get_db_connection_or_die();

$name = $_POST['name'];
$surname = $_POST['surname'];

//Se alamacenan dos variables email para que se compruebe este campo y evitar errores del ususario
$email = $_POST['email'];
$email_confirmation = $_POST['email_confirmation'];

//Sucede los mismo que con el campo email
$password = $_POST['password'];
$password_confirmation = $_POST['password_confirmation'];

$business_name = $_POST['business_name'];
$number = $_POST['number'];
$country_code = $_POST['country_code'];
$profile_type = $_POST['profile_type'];

//Se comprueba que todos los campos del formulario se han rellenado
if (empty($name) or empty($surname) or empty($email) or empty($email_confirmation) or empty($password) or empty($password_confirmation) or empty($business_name) or empty($number) or empty($country_code)) {
	die("<h1>Faltan datos!!!</h1>");
}

//Se realiza la comprobación del email
if ($email != $email_confirmation){
	die("<h1>No se ha confimado el email correctamente</h1>");
}

//Se realiza la comprobación de la contraseña
if($password != $password_confirmation) {
	die("<h1>No se ha confimado la contraseña correctamente</h1>");
}

//Realizamos un prepare statement con la sentencia SQL necesaria, en este caso un INSERT a la BD
//Este será dispuesto dentro de un "try-catch" por si aparecen errores en la ejecución

try{
	$sql = "INSERT INTO tUser (name, surname, email, encrypted_password, business_name, business_phone_number, business_phone_country_code, profile_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	$stmt = $mysqli -> prepare($sql);

	//La contraseña será encryptada y posteriormente hasheada
	$password = password_hash($_POST['password'], PASSWORD_BCRYPT);

	$stmt -> bind_param("ssssssss", $name, $surname, $email, $password, $business_name, $number, $country_code, $profile_type);
	$stmt -> execute();


	if (!empty($mysqli -> error)){
		echo($mysqli -> error);
		header("Location: register.php?register_failed_email=True");
		exit();
	}


	$stmt -> close();
} catch(Exception $e) { //Si algo ha ido mal, saltará este error, el cual manda un header diferente a los demas errores
	error_log($e);
	header("Location: register.php?register_failed_unknown=True");
	exit();
}
//Si todo ha ido correctamente, "success" será True, esta información será enviada a traves de la url, y se recibirá en register.php, lo que pintará un mensage en pantalla.
header("Location: register.php?success=True");
