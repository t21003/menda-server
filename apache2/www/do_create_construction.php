<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

session_start();
$mysqli = get_db_connection_or_die();

$asociar_id = $_SESSION['user_id'];
$build_name = $_POST['f_building_name'];
$architect = $_POST['f_architect'];
$hirer = $_POST['f_hirer'];
$start_date = $_POST['f_start_date'];
$end_date = $_POST['f_end_date'];
$address = $_POST['f_address'];
$latitude = $_POST['f_latitude'];
$longitude = $_POST['f_longitude'];
$fechaActual = date('Y-m-d');

// Si la longitud o latitud es un string que nos devuelva un error.
if(is_numeric($latitude) and is_numeric($longitude)){
    $latitude = doubleval($latitude);
    $longitude = doubleval($longitude);
}else{ 
    ?>
     <!DOCTYPE html>
        <html lang="es-ES">
            <head>
                <meta charset="UTF-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
                <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet" />
                <link href="./static/estilos.css" rel="stylesheet" type="text/css" />
                <title>Volver a la página principal</title>
            </head>

            <body class="pag_error">
                <div class="header">
                    <h2 class="blanco">MENDA</h2>
                </div>
                <div class="content">
                    <div class="section">
                        <h3 class="fh3">
                            ERROR! La latitud y longitud no puede contener letras!
                        </h3>
                        <a href="./create_construction.php?">Volver al formulario</a>
                    </div>
                </div>
                <div class="footer_fallo">
                    <p class="blanco">© Menda</p>
                </div>
            </body>
        </html>
    <?php
    die();
    }
    //Si la fecha inicial es menor que la actual  o la fecha final es mayor a la actual no va a poder crear la obra.
    if($start_date < $fechaActual || $start_date > $end_date){
    ?>
       <!DOCTYPE html>
        <html lang="es-ES">
            <head>
                <meta charset="UTF-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
                <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet" />
                <link href="./static/estilos.css" rel="stylesheet" type="text/css" />
                <title>Volver a la página principal</title>
            </head>

            <body class="pag_error">
                <div class="header">
                    <h2 class="blanco">MENDA</h2>
                </div>
                <div class="content">
                    <div class="section">
                        <h3 class="fh3">
                            Error! Fechas incorrectas!
                        </h3>
                        <p class="blanco">Posibilidades:</p>
                        <p class="blanco">La fecha actual no puede ser menor a la fecha inicial</p>
                        <p class="blanco">La fecha final no puede ser menor a la fecha inicial</p>
                        <a href="./create_construction.php?">Volver al formulario</a>
                    </div>
                </div>
                <div class="footer_fallo">
                    <p class="blanco">© Menda</p>
                </div>
            </body>
        </html>
    <?php
    die();
    }
    // }else if(is_string($latitude) && is_string($latitude)) {
    //     die("No puede ser un string");
    // }

    try {
        $stmt = $mysqli->prepare("INSERT INTO tConstruction (building_name , architect , hirer , start_date, end_date,latitude,longitude, address, author_id) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssssddsi", $build_name, $architect, $hirer, $start_date, $end_date,$latitude ,$longitude, $address, $asociar_id);
        $stmt->execute();
        // echo $stmt -> error;
        header('Location: main.php');
        $stmt->close();
        
    } catch (Exception $e) {
        error_log($e);
        header('Location: create_construction.php?failed=True');
    }
    $mysqli->close();

?>
