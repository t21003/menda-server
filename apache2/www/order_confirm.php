<!DOCTYPE html>
<html lang="es_ES">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./sprint7.css" />
    <title>Confirmación de pedido</title>
</head>

<body>
    <?php

    use FFI\CType;

    // ini_set('display_errors', 'On');

    require __DIR__ . '/../php_util/db_connection.php';
    $mysqli = get_db_connection_or_die();
    session_start();
    $result = mysqli_query(
        $mysqli,
        "SELECT * FROM tOrderItem inner join tOrder on tOrder.id = tOrderItem.order_id inner join tConstruction on tOrder.construction_id = tConstruction.id where tConstruction.author_id = " . $_SESSION['user_id'] . " and tOrder.id = " . $_get['order_id']
    );
    
    $i = 0;
    echo '<h1>Detalles del pedido</h1>';
    echo '<form action="/do_order_confirm.php?order_id=' . $_GET['order_id'] . '" method="post">';
    while ($row = mysqli_fetch_array($result)) {
        $i++;

        echo '<p>' . $row['item_name'] . '</p>';
        echo '<p>' . $row['quantity'] . '</p>';
        echo '<p>' . $row['measurement_unit'] . '</p>';
        echo '<p>' . $row['price'] . '</p>';
        echo '<br>';
    }

    if ($i !== 0) {

        echo '<button type="submit">Confirmar</button>';

        echo '</form>';
    }


    ?>
</body>

</html>