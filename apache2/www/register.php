<?php
	ini_set('display_errors', 'On');
?>

<!DOCTYPE html>
<html lang="es-ES" dir="ltr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./sprint7.css" />
    <title>Menda</title>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="./script.js"></script>
</head>

<body>
    <div class="logo" style="margin-bottom: 40px; margin-top: 40px;">
        <img src="static/menda.png" width="110vw" height="auto" alt="mascotasplus logo">
    </div>
    <div class="wrapper">
        <div class="title-text">
            <div class="title login">Registro</div>
        </div>
        <!--Errores que se mostraran en caso de que se produzcan-->
        <?php
		//Mensaje de éxito cuando el usuario ha sido creado correctamente.
        if(isset($_GET['success'])) {
            //Este mensaje se mostrará si todo ha ido correctamente, es decir, success=True en el header.
            if($_GET['success'] == TRUE) {
                echo '<p id="success-log">Usuario creado correctamente</p>';
            }
        }
        if(isset($_GET['success'])) {
            //Este mensaje se mostrará si todo ha ido correctamente, es decir, success=True en el header.
            if($_GET['success'] == TRUE) {
                echo '
                <div id="container-link-to-login">
                    <a id="link-to-login" href="login.php";>Inciar sesión</a>
                </div>';
            }
        }
        if(isset($_GET['register_failed_email'])) {
            if($_GET['register_failed_email'] == TRUE){//Si se da el caso de que ya exista un usuario con el email introducido, se mostrará este menjage por pantalla
                echo '<p id="error-log">El email ya ha sido registrado anteriormente</p>';
            }
        }
        //
        if(isset($_GET['register_failed_unknown'])) {
            if($_GET['register_failed_unknown'] == TRUE) {//Si sucedió un error desconocido se mostrará este mensage por pantalla
                 echo '<p id="error-log">Error desconozido. Si el error persiste, inténtalo más tarde.</p>';
             }
         }
        ?>
        <div class="form-container">
            <div class="form-inner">
                <form action="do_register.php" method="post" class="login">
                    <div class="field">
                        <input name="name" id="name" type="text" placeholder="Nombre" required />
                    </div>
                    <div class="field">
                        <input name="surname" id="surname" type="text" placeholder="Apellidos" required />
                    </div>
                    <div class="field">
                        <input name="email" id="email" type="text" placeholder="Email" required />
                    </div>
                    <div class="field">
                        <input name="email_confirmation" id="email_confirmation" type="text" placeholder="Confirmar email" onpaste="return false;" ondrop="return false;" autocomplete="off" required />
                    </div>
                    <div class="field">
                        <input name="password" id="password" type="password" placeholder="Contraseña" autocomplete="off" required />
                    </div>
                    <div class="field">
                        <input name="password_confirmation" id="password_confirmation" type="password" placeholder="Confirmar contraseña" onpaste="return false;" ondrop="return false;" autocomplete="off" required />
                    </div>
                    <div class="field">
                        <input name="business_name" id="business_name" type="text" placeholder="Nombre de negocios" required />
                    </div>
                    <div class="field">
                        <input name="number" id="number" type="text" placeholder="Número de teléfono" required />
                    </div>
                    <div class="field">
                        <input name="country_code" id="country_code" type="text" placeholder="Código de país" required />
                    </div>
                    <div class="field">
                        <select id="profile_type" name="profile_type">
                            <option value="" disabled selected>Tipo de usuario</option>
                            <option value="builder">Constructor</option>
                            <option value="provider">Proveedor</option>
                        </select>
                    </div>
                    <div class="field btn">
                        <div class="btn-layer"></div>
                        <input type="submit" value="Registrarse" />
                    </div>
                    <div class="signup-link">
                        ¿Ya estás registrado? <a href="login.php">Inicia sesión</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>