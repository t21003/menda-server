<?php
// Conexión con la base de datos
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

$mysqli = get_db_connection_or_die();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Href llama ahora a una ruta absoluta ya que la ruta relativa generaba errores-->
    <link rel="stylesheet" href="/static/stylesraquel.css" type="text/css"> 
    <title>Crear Pedido</title>
</head>

<body>
    <?php
    session_start();
    $user_id = $_SESSION['user_id'];

    $id_obra = '';
    if (isset($_GET['id'])) {
        $id_obra = $_GET['id'];
    }

    // Comprobar si ha llegado el parámetro failed=True
    if (isset($_GET['failed'])) {
        $failed = $_GET['failed'];
        if ($failed) {
            echo "<p class='error'>La creación del pedido ha fallado</p>";
        }
    }

    // Comprobar que la sesión del usuario está activa
    if (empty($user_id)) {
        echo "No se encuentra el user_id";
        echo "<br><a href=/login.php>Volver</a>";
    } else {
        // Consulta a la base de datos del tipo de usuario que tiene iniciada sesión
        $query = 'SELECT * FROM tUser WHERE id=' . $user_id;
        $result = mysqli_query($mysqli, $query) or die('Query error');
        $only_row = mysqli_fetch_array($result);
        $profile_type = $only_row['profile_type'];

        if ($profile_type == "builder") {
    ?>
            <div class="background">
                <div class="imageForm">
                    <h1>Crear pedido</h1>
                    <form action="/do_create_order.php/?construction_id=<?php echo ($id_obra); ?>" method="POST">
                        <label for="item_name">Nombre del producto<span class='required'>*</span></label><br>
                        <input type="text" id="item_name" name="item_name" required><br>
                        <label for="quantity">Cantidad<span class='required'>*</span></label><br>
                        <input type="number" id="quantity" name="quantity" min="1" required><br>
                        <label for="measurement_unit">Unidad de medida<span class='required'>*</span></label><br>
                        <input type="text" id="measurement_unit" name="measurement_unit" required><br>

                        <!--El builder puede escoger un proovedor de la bbdd -->
                        <label for="provider_id">Escoge un proveedor <span class='required'>*</span></label><br>
                        <select id="provider_id" name="provider_id" required>

                            <?php
                            $stmt = $mysqli->prepare("SELECT id, name FROM tUser WHERE profile_type='provider'");
                            $stmt->execute();
                            $result = $stmt->get_result();
                            while ($fila = $result->fetch_row()) {
                                echo "<option value=$fila[0]>$fila[1]</option>";
                            }
                            ?>

                        </select><br><br>
                        <input type="submit" value="SOLICITAR">
                    </form>
                </div>
            </div>
    <?php
        } elseif ($profile_type == "provider") {
            echo "Un usuario que es proveedor no puede registrar pedidos";
        }
    }

    mysqli_close($mysqli);
    ?>
</body>

</html>