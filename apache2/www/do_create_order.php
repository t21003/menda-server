<?php
    // Conexión con la base de datos
    ini_set('display_errors', 'On');
    require __DIR__ . '/../php_util/db_connection.php';

$mysqli = get_db_connection_or_die();
?>

<!DOCTYPE html>
<html>
<body>

<?php
session_start();

$user_id = $_SESSION['user_id'];

//Update: recoger lo que viene por el navegador
$construction_id = '';
if (isset ($_GET['construction_id'])){
    $construction_id = $_GET['construction_id'];
}


// Recogemos los parámetros enviados desde el formulario de create_order.php
$item_name = $_POST['item_name'];
$quantity = $_POST['quantity'];
$measurement_unit = $_POST['measurement_unit'];
$provider_id = $_POST['provider_id'];


// Prepared statements para insertar los datos recogidos del formulario a las tablas tOrder y tOrderItem 
$stmt = $mysqli->prepare("INSERT INTO tOrder (construction_id, provider_id) VALUES (?, ?)");
$stmt->bind_param("ii", $construction_id, $provider_id); 

$stmt->execute();

// Almacenar el id autogenerado en la tabla tOrder para insertarlo en la tabla tOrderItem
$id = $stmt->insert_id;

$stmt2 = $mysqli->prepare("INSERT INTO tOrderItem (order_id, item_name, quantity, measurement_unit) VALUES (?, ?, ?, ?)");
$stmt2->bind_param("isis", $id, $item_name, $quantity, $measurement_unit);

$stmt2->execute();

//Update: mejor que compruebe ambas querys
if ($mysqli->affected_rows>0){
    header("Location: /main.php");
    exit;
}  
else {
    header("Location: /create_order.php?failed=True");
    exit;
}



/* Comprobar que las sentencias se han ejecutado con éxito
if ($stmt->execute() && $stmt2->execute()) {
    header("Location: /main.php");
    exit;
} else {
    header("Location: /create_order.php?failed=True");
    exit;
}
*/
$stmt->close();
$stmt2->close();
$mysqli -> close();

?>

</body>
</html>
