from django.http import JsonResponse, HttpResponseRedirect
import json
from django.views.decorators.csrf import csrf_exempt
from .models import Tconstruction, Torder, Tuser


# Función para get request de construcciones
@csrf_exempt
def get_constructions(request):
    if request.method == "GET":
        # Recuperar datos del header y la url
        my_token = request.headers.get(
            "token"
        )  # Ahorro las lineas que requiere el try-except
        my_author_user_id = request.GET.get("author_user_id", "")
        my_provider_user_id = request.GET.get("provider_user_id", "")
        array_constructions = []
        # Comprobar si el token no esta vacío
        if my_token:
            # Comprobar si existe el param author_user_id
            if my_author_user_id:
                # Comprobar veracidad del token
                if (
                    Tuser.objects.get(id=my_author_user_id).active_session_token
                    == my_token
                ):
                    # Filtrar construcciones
                    for construction in Tconstruction.objects.filter(
                        author_id=my_author_user_id
                    ):
                        dict_construccion = {}
                        dict_construccion["id"] = construction.id
                        dict_construccion["building_name"] = construction.building_name
                        dict_construccion["architect"] = construction.architect
                        dict_construccion["hirer"] = construction.hirer
                        dict_construccion["start_date"] = construction.start_date
                        dict_construccion["end_date"] = construction.end_date
                        array_constructions.append(dict_construccion)
                    return JsonResponse(
                        status=200, data=array_constructions, safe=False
                    )
                else:
                    return JsonResponse(
                        status=403,
                        data={
                            403: "La petición ha fallado porque el token de autenticación no es válido, o bien no está asociado al 'author_user_id' ó enviado"
                        },
                    )
            elif my_provider_user_id:
                if (
                    Tuser.objects.get(id=my_provider_user_id).active_session_token
                    == my_token
                ):
                    for order in Torder.objects.filter(provider_id=my_provider_user_id):
                        for construction in Tconstruction.objects.filter(
                            id=order.construction_id
                        ):
                            dict_construccion = {}
                            dict_construccion["id"] = construction.id
                            dict_construccion["building_name"] = construction.building_name
                            dict_construccion["architect"] = construction.architect
                            dict_construccion["hirer"] = construction.hirer
                            dict_construccion["start_date"] = construction.start_date
                            dict_construccion["end_date"] = construction.end_date
                            array_constructions.append(dict_construccion)

                    return JsonResponse(
                        status=200, data=array_constructions, safe=False
                    )
                else:
                    return JsonResponse(
                        status=403,
                        data={
                            403: "La petición ha fallado porque el token de autenticación no es válido, o bien no está asociado al'provider_user_id' enviado"
                        },
                    )
            else:
                return JsonResponse(
                    status=400,
                    data={
                        400: "La petición ha fallado porque no se ha especificado 'author_user_id' ó 'provider_user_id'."
                    },
                )
        else:
            return JsonResponse(
                status=401,
                data={
                    401: "La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras"
                },
            )

    elif request.method == "POST":
        token = request.headers.get("token")
        user_id = ""
        try:
            user_id = Tuser.objects.get(active_session_token=token).id
        except Tuser.DoesNotExist:
            return JsonResponse(
                status=403,
                data={
                    403: "La petición ha fallado porque el token de sesión no es válido"
                },
            )

        body_data = {}
        if not token:
            return JsonResponse(
                status=401,
                data={
                    401: "La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras"
                },
            )
        try:
            body_data = json.loads(request.body)
            construction = Tconstruction(
                building_name=body_data["building_name"],
                architect=body_data["architect"],
                hirer=body_data["hirer"],
                start_date=body_data["start_date"],
                end_date=body_data["end_date"],
                address=body_data["location"]["address"],
                latitude=body_data["location"]["latitude"],
                longitude=body_data["location"]["longitude"],
                author_id=user_id,
            )
            construction.save()
            return JsonResponse(status=201, data={201: "Creado con éxito."})

        except KeyError:
            return JsonResponse(
                status=400,
                data={
                    400: "La petición ha fallado porque alguno de los parámetros no está o es inválido"
                },
            )
    else:
        return JsonResponse(status=405, data={405: "Method Not Allowed"})
