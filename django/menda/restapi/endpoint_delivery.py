from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import smtplib, ssl
from email.mime.text import MIMEText
import json
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from .models import Tuser
from .models import Tconstruction
from .models import Torder
from .models import Torderitem

@csrf_exempt
def signature_delivery(request, id, order_id):
    try:
    # Recogemos el token y comprobamos que no venga nulo
        token = request.headers['token']
    except KeyError:
        msgdata = {
            "code":401,
            "codemsg":'La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras'
        }
        return JsonResponse(status=401,data=msgdata)
    # Buscamos la obra y el pedido por su id y si no existen devolvemos el error 404
    try:
        obra = Tconstruction.objects.get(id=id)
        pedido = Torder.objects.get(id=order_id)
    except:
        msgdata = {
            "code":404,
            "codemsg":'La petición ha fallado porque no existe una obra con el ID especificado o un pedido con el ID especificado'
        }
        return JsonResponse(status=404, data=msgdata)
    # Comprobamos que el token sea el de la sesión activa del constructor
    constructor = obra.author
    #  constructor = Tuser.objects.get(id=constructor_id)
    if token != constructor.active_session_token :
        msgdata = {
            "code":403,
            "codemsg":'La petición ha fallado porque el token de sesión no se corresponde con una sesión activa del Constructor asociado al pedido'
        }
        return JsonResponse(status=403,data=msgdata)
    #Comprobamos si el pedido esta en confirmed
    if pedido.status != 'confirmed':
        msgdata = {
            "code":409,
            "codemsg":'La petición ha fallado porque el pedido no está en estado confirmed'
            }
        return JsonResponse(status=409, data=msgdata)
    #Leemos el body de la request y nos guardamos sus valores
    body = json.loads(request.body)
    orderitems = body['order_items_acceptance']
    signature_constructor = body['builder_signature']
    signature_transporter = body['carrier_signature']
    cantidades = []
    itemspedido = Torderitem.objects.filter(order=pedido.id)
    #Guardamos en "cantidades" las accepted_quantity para compararlas luego
    for item in orderitems:
        cantidades.append(item['accepted_quantity'])
    #Aprovechamos el siguiente bucle para construir la string del albarán que usaremos para el mail
    stringAlbaran = ""
    #Comprobamos si las cantidades aceptadas son menores a 0 o mayores al numero inicial del item
    for num in range(len(itemspedido)):
        stringAlbaran = stringAlbaran + "\n"+"Producto " +str(num)+ " , Cantidad :" +str(cantidades[num])
        if cantidades[num] < 0 or cantidades[num]>itemspedido[num].quantity:
                msgdata = {
                        "code":409,
                        "codemsg":'La petición ha fallado porque alguna de las cantidades especificadas es menor a 0 o mayor a la cantidad total asociada a dicho item'
                        }
                return JsonResponse(status=409,data=msgdata)
    # Todo correcto hacemos los updates de valores
    pedido.delivery_builder_signature = signature_constructor
    pedido.delivery_carrier_signature = signature_delivery
    pedido.status = 'delivered'
    pedido.save()
    for count in range(len(itemspedido)):
        itemspedido[count].accepted_quantity = orderitems[count]['accepted_quantity']
        itemspedido[count].save()
    #Construimos el  mail del albarán
    sender_email = "t2notifications_no_reply@fpcoruna.afundacion.org"
    receiver_email = pedido.provider.email
    password = "Password"
    subject = "Albaran"
    salto = "\n"
    body = """\
    Pedido ID : """+str(pedido.id)+salto+"""

    Se ha entregado el albaran con el pedido indicado con los siguientes productos y cantidades:"""+stringAlbaran+salto+ """\
    Fin del pedido.
    Si no esta conforme con los datos del albaran mande un mail a : fakemail@wedontreply.com"""
    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    # Add body to email
    message.attach(MIMEText(body, "plain"))
    text = message.as_string()
    #Creamos la conexión al servidor y mandamos el mensaje
    with smtplib.SMTP("smtp.gmail.com") as server:
    	server.connect("smtp.gmail.com",587)
    	server.ehlo()
    	server.starttls()
    	server.ehlo()
    	server.login(sender_email, password)
    	server.sendmail(sender_email, receiver_email, text)

    msgdata = {
            "code":200,
            "codemsg":'Los datos de aceptación del albarán se han subido con éxito.'
        }
    return JsonResponse(status=200, data=msgdata)
